package com.umg.curso;

import java.util.Scanner;

public class App {
    private static Tienda tienda;

    public static void main(String[] args) {
        tienda = new Tienda();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ingrese el nombre de la tienda:");
        tienda.setNombreGerente(scanner.next());
        System.out.println("\nIngrese la dirección de la tienda:");
        tienda.setDireccion(scanner.next());
        System.out.println("\nIngrese el año de publicación de la tienda:");
        tienda.setAnnoCreacion(scanner.nextInt());
        System.out.println("\nIngrese la cantidad de mascotas a registrar:");
        int cantMascotas = scanner.nextInt();
        for (int i=0; i<cantMascotas; i++) {
            Mascota mascota = new Mascota();
            System.out.println("\nIngrese el nombre de la mascota:");
            mascota.setNombre(scanner.next());
            System.out.println("\nIngrese el tipo de animal:");
            mascota.setTipoAnimal(scanner.next());
            System.out.println("\nIngrese la raza de animal:");
            mascota.setRaza(scanner.next());
            System.out.println("\nIngrese la edad del animal:");
            mascota.setEdad(scanner.nextInt());
            tienda.agregarMascota(mascota);
        }
        System.out.println("\n\n");
        System.out.println("LISTADO DE MASCOTAS:");

        for(int i=0; i< (tienda.getMascotas()).size(); i++) {
            Mascota mascota = (Mascota) (tienda.getMascotas()).get(i);
            System.out.println("No."+(i+1)+" - Nombre:"+mascota.getNombre()+" - Tipo de Mascota:"+mascota.getTipoAnimal()+" - Raza:"+mascota.getRaza()+"  - Edad:"+mascota.getEdad());
        }

        System.out.println("La suma de las edades de las mascotas es:" + tienda.sumaAñosAnimales());
    }
}
