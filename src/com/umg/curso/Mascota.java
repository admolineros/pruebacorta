package com.umg.curso;

public class Mascota {
    private String tipoAnimal;
    private String raza;
    private String nombre;
    private int edad;

    public Mascota() {
    }

    public Mascota(String tipoAnimal, String raza, String nombre, int edad) {
        this.tipoAnimal = tipoAnimal;
        this.raza = raza;
        this.nombre = nombre;
        this.edad = edad;
    }

    public String getTipoAnimal() {
        return tipoAnimal;
    }

    public void setTipoAnimal(String tipoAnimal) {
        this.tipoAnimal = tipoAnimal;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}
