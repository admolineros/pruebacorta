package com.umg.curso;

import java.util.ArrayList;

public class Tienda {
    private String nombreGerente;
    private String direccion;
    private int annoCreacion;
    private ArrayList mascotas;

    public Tienda() {
        mascotas = new ArrayList();
    }

    public Tienda(String nombreGerente, String direccion, int annoCreacion, ArrayList mascota) {
        this.nombreGerente = nombreGerente;
        this.direccion = direccion;
        this.annoCreacion = annoCreacion;
        this.mascotas = mascota;
    }

    public String getNombreGerente() {
        return nombreGerente;
    }

    public void setNombreGerente(String nombreGerente) {
        this.nombreGerente = nombreGerente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getAnnoCreacion() {
        return annoCreacion;
    }

    public void setAnnoCreacion(int annoCreacion) {
        this.annoCreacion = annoCreacion;
    }

    public ArrayList getMascotas() {
        return mascotas;
    }

    public void setMascotas(ArrayList mascota) {
        this.mascotas = mascota;
    }

    public void agregarMascota(Mascota mascota) {
        mascotas.add(mascota);
    }

    public int sumaAñosAnimales() {
        int suma = 0;
        for(int i=0; i< mascotas.size(); i++) {
            suma += ((Mascota) mascotas.get(i)).getEdad();
        }
        return suma;
    }
}
